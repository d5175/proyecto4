<?php

namespace app\controllers;

use Yii;
use app\models\Etapa;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EtapaController implements the CRUD actions for Etapa model.
 */
class EtapaController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Etapa models.
     *
     * @return string
     */
    public function actionIndex() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $dataProvider = new ActiveDataProvider([
                'query' => Etapa::find(),
                    /*
                      'pagination' => [
                      'pageSize' => 50
                      ],
                      'sort' => [
                      'defaultOrder' => [
                      'numetapa' => SORT_DESC,
                      ]
                      ],
                     */
            ]);

            return $this->render('index', [
                        'dataProvider' => $dataProvider,
            ]);
        }
    }

    /**
     * Displays a single Etapa model.
     * @param int $numetapa Numetapa
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($numetapa) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            return $this->render('view', [
                        'model' => $this->findModel($numetapa),
            ]);
        }
    }

    /**
     * Creates a new Etapa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = new Etapa();

            if ($this->request->isPost) {
                if ($model->load($this->request->post()) && $model->save()) {
                    return $this->redirect(['view', 'numetapa' => $model->numetapa]);
                }
            } else {
                $model->loadDefaultValues();
            }

            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Etapa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $numetapa Numetapa
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($numetapa) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $model = $this->findModel($numetapa);

            if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'numetapa' => $model->numetapa]);
            }

            return $this->render('update', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Etapa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $numetapa Numetapa
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($numetapa) {
        if (Yii::$app->user->isGuest) {
            return $this->goHome();
        } else {
            $this->findModel($numetapa)->delete();

            return $this->redirect(['index']);
        }
    }

    /**
     * Finds the Etapa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $numetapa Numetapa
     * @return Etapa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($numetapa) {
        if (($model = Etapa::findOne(['numetapa' => $numetapa])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
