<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'MAD Awards 2022';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: 0px">
        <h1 class="display-4">CRUD MANAGEMENT</h1>

        <p class="lead">Here you can create, read, update or delete registers from the app</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row">
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_cyclist.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Cyclists', ['ciclista/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_team.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Teams', ['equipo/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_stage.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Stages', ['etapa/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card center  fame-card">
                <?= Html::img("@web/images/A_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Maillots', ['maillot/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
                        <div class="card center  fame-card">
                <?= Html::img("@web/images/port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Ports', ['puerto/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
                        <div class="card center  fame-card">
                <?= Html::img("@web/images/carries.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <?= Html::a('Carries', ['lleva/index'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
        </div>

    </div>
</div>