<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'MOST WANTED MAILLOTS';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white" Style="margin-bottom: -30px">
        <h1 class="display-4">MOST WANTED MAILLOTS CATEGORY AWARDS</h1>

        <p class="lead">You may see me struggle, but you will never see me quit.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row" Style="justify-content: space-around">

            <div class="card text-center fame-card col-4">
                <?= Html::img("@web/images/A_maillot.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Most valued maillot Award</h5>
                    <?= Html::a('View', ['maillot/mvm'], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">
            <?= Html::a('Print dossier', ['site/print'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Go back', ['site/index'], ['class' => 'btn btn-warning']) ?>
        </div>

    </div>
</div>