<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'WORST TEAMS';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white comic" Style="margin-bottom: -30px">
        <h1 class="display-4 comic">WORST TEAMS CATEGORY AWARDS</h1>

        <p class="lead">Teamwork is essential, it allows you to blame someone else.</p>

    </div>

    <div class="body-content">

        <div class="card-deck flex-row" Style="justify-content: space-around">

            <div class="card text-center  shame-card col-4">
                <?= Html::img("@web/images/B_stages.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Least stages won Award</h5>
                    <?= Html::a('View', ['equipo/lsw'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <div class="card text-center  shame-card col-4">
                <?= Html::img("@web/images/w_director.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Worst director Award</h5>
                    <?= Html::a('View', ['equipo/wda'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">
            <?= Html::a('Print dossier', ['site/print'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Go back', ['site/shame'], ['class' => 'btn btn-danger']) ?>
        </div>

    </div>
</div>
