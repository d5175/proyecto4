<?php
/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'WORST CYCLYSTS';
?>
<div class="site-index">

    <div class="jumbotron text-center bg-transparent text-white comic" Style="margin-bottom: -30px">
        <h1 class="display-4 comic">WORST CYCLYSTS CATEGORY AWARDS</h1>

        <p class="lead">Hard work has a future payoff. Laziness pays off now.</p>
        
    </div>

    <div class="body-content">

        <div class="card-deck flex-row">

            <div class="card text-center  shame-card">
                <?= Html::img("@web/images/B_stages.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Least stages won Award</h5>
                    <?= Html::a('View', ['ciclista/lsw'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <div class="card text-center  shame-card">
                <?= Html::img("@web/images/maillot_grayed.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Least maillots won Award</h5>
                    <?= Html::a('View', ['lleva/lmw'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

            <div class="card text-center  shame-card">
                <?= Html::img("@web/images/B_port.png", ['class' => 'resize']) ?>
                <div class="card-body">
                    <h5 class="card-title">Least ports won Award</h5>
                    <?= Html::a('View', ['port/lpw'], ['class' => 'btn btn-danger']) ?>
                </div>
            </div>

        </div>

        <div class="jumbotron text-right bg-transparent text-white d-flex col-12" Style="justify-content: space-between; padding-bottom: 1px; margin-bottom: 0px ">
            <?= Html::a('Print dossier', ['site/print'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Go back', ['site/shame'], ['class' => 'btn btn-danger']) ?>
        </div>

    </div>
</div>